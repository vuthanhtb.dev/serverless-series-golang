### Build
```
GOOS=linux go build -o hello main.go
zip main.zip hello
```

### Run
```
aws lambda update-function-code --function-name hello-lambda --zip-file fileb://./main.zip
```

### Test
```
aws lambda invoke --function-name hello-lambda --region ap-southeast-1 response.json
```
