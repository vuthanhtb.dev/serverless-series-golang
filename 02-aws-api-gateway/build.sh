#!/bin/bash

go build -o main main.go
zip list.zip main
rm -rf main

# aws lambda update-function-code --function-name books_list --zip-file fileb://list.zip --region ap-southeast-1