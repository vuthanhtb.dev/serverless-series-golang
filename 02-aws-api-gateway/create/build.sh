#!/bin/bash

GOOS=linux go build -o main main.go
zip create.zip main
rm -rf main

# aws lambda create-function --function-name books_create --zip-file fileb://create.zip --runtime go1.x --handler main --role arn:aws:iam::ACCOUNT_ID:role/lambda_role --region ap-southeast-1
