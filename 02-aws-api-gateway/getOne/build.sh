#!/bin/bash

GOOS=linux go build -o main main.go
zip getOne.zip main
rm -rf main

# aws lambda create-function --function-name books_get_one --zip-file fileb://getOne.zip --runtime go1.x --handler main --role arn:aws:iam::ACCOUNT_ID:role/lambda_role --region ap-southeast-1
